﻿Imports System.IO
Imports System.IO.Compression

Public Class clsUtil


    Public Shared Function CompimirGZipBase64(ByVal textoEntrada As String) As String
        Dim base64String As String = ""

        Try

            Dim comprimido As Byte()
            Using memOut = New MemoryStream()
                Using gzipLector = New GZipStream(memOut, CompressionMode.Compress)
                    Using memCompresor As New MemoryStream(Encoding.UTF8.GetBytes(textoEntrada))
                        memCompresor.CopyTo(gzipLector)
                    End Using
                End Using

                comprimido = memOut.ToArray()
                base64String = Convert.ToBase64String(comprimido)

            End Using

        Catch ex As Exception
            clsLog.Log.Fatal(ex.Message & " en " & System.Reflection.MethodBase.GetCurrentMethod().Name)
        Finally

        End Try

        Return base64String

    End Function

    Public Function getNextCodeTabla(ByVal NombreTabla As String, ByRef oCon As Odbc.OdbcConnection) As String
        Dim retVal As String = ""
        Dim SQL As String = ""

        Try

            SQL = "Select ISNULL(MAX(CAST(Code as numeric)),0) + 1 from " & NombreTabla

            Dim oCommand As Odbc.OdbcCommand = oCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CStr(oCommand.ExecuteScalar())
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally

        End Try

        Return retVal

    End Function

    Public Function getCorreosEmpleado(ByVal Empleado As Integer) As String
        Dim retVal As String = ""
        Dim SQL As String = ""
        Dim mCon As Odbc.OdbcConnection = Nothing

        Try
            mCon = New clsConexion().OpenConexionSQLSAP

            'Siempre separados por comas
            SQL = "SELECT REPLACE(ISNULL(U_SEIMails,''), ';',',') as U_SEIMails FROM OHEM where EmpID='" & Empleado & "'"

            Dim oCommand As Odbc.OdbcCommand = mCon.CreateCommand
            oCommand.Parameters.Clear()
            oCommand.CommandText = SQL

            retVal = CStr(oCommand.ExecuteScalar())
            oCommand = Nothing

        Catch ex As Exception
            clsLog.Log.Error(ex.Message & " (SQL:" & SQL & ") en " & System.Reflection.MethodBase.GetCurrentMethod().Name, ex)
        Finally
            If Not IsNothing(mCon) Then
                If mCon.State = ConnectionState.Open Then
                    mCon.Close()
                End If
            End If
        End Try

        Return retVal

    End Function


End Class