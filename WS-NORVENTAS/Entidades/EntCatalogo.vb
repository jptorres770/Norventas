﻿Option Strict On
Public Class EntCatalogo

    Public Property IDUnicoCatalogo As Long
    Public Property NombreCatalogo As String
    Public Property CardCode As String
    Public Property CardName As String
    Public Property FechaCreacion As Integer
    Public Property Notas As String
    Public Property EmpID As Integer
    Public Property Sincro As String
    Public Property ErrorSincro As String
    Public Property Borrado As String

End Class
