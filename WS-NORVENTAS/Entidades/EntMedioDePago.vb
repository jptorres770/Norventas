﻿Public Class EntMedioDePago

    Public Property Tipo As String
    Public Property Importe As Double
    Public Property CuentaTransferencia As String
    Public Property CuentaCheque As String
    Public Property Referencia As String
    Public Property Fecha As String
    Public Property FechaVencimiento As String
    Public Property CodBanco As String
    Public Property NumCheque As Integer
    Public Property Endosado As String

End Class
