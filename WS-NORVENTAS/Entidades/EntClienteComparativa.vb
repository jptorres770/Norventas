﻿Public Class EntClienteComparativa

    Public Property CardCode As String
    Public Property CardName As String
    Public Property EmpID As Integer
    Public Property ImpActual As Double
    Public Property ImpAnterior As Double
    Public Property Diferencia As Double
    Public Property PorcDiferencia As Double
End Class
