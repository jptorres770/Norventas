﻿Imports Entidades

Public Class SEI_CalculoDescuentos

    Private Const lCodigoDescuento As String = "U_SEIDescD"
    Private Const lDescuento1 As String = "U_SEIDesc1"
    Private Const lDescuento2 As String = "U_SEIDesc2"
    Private Const lDescuento3 As String = "U_SEIDesc3"
    Private Const lDescuento4 As String = "U_SEIDesc4"
    Private Const lDescuento5 As String = "U_SEIDesc5"
    Private Const lPrecioNeto As String = "U_SEIPrice"


    Public Sub AsignarDescuentoLin_DI(ByRef oCab As EntDocumentoCab, _
                                 ByRef oLin As EntDocumentoLin, 
                                 ByRef oCompany As SAPbobsCOM.Company)
        '
        Dim sFecha As String
        Dim sArticulo As String
        Dim sCliente As String
        Dim dCantidad_Lin As Double
        Dim iRegistro As Long
        '
        Dim oDescuento As New SEI_DescuentosDI()
        '
        sFecha = CStr(oCab.DocDate) ' 
        sCliente = oCab.CardCode
        sArticulo = oLin.ItemCode
        dCantidad_Lin = oLin.CantidadReal
        '
        iRegistro = oDescuento.DESCUENTO_LINIA(sArticulo, _
                                               sCliente, _
                                               sFecha, _
                                               dCantidad_Lin, _
                                               SEI_DescuentosDI.TIPOVENTASoCOMPRAS.TVentas, _
                                               oCompany)
        '
        Dim ls As String
        '
        '----------------------------------------------------------------------
        ' Esta asignacion se simplementa para inicializar las variables
        '----------------------------------------------------------------------
        ' 
        ls = ""
        ls = ls & " SELECT Code, U_SEIDesc1, U_SEIDesc2, U_SEIDesc3, U_SEIDesc4, U_SEIDesc5,U_SEIPrice"
        ls = ls & " FROM [@SEIDESCUENTOSCLI] "
        ls = ls & " WHERE DocEntry = " & iRegistro.ToString
        '
        If Not ObtenerLinea_SEIDESC(ls, oLin, oCompany) Then
            '
            oLin.CodigoDescuento = "0"
            oLin.dto1 = 0
            oLin.dto2 = 0
            oLin.dto3 = 0
            oLin.dto4 = 0
            oLin.dto5 = 0
            oLin.Precio = 0
            '
        Else
            If NullToDoble(oLin.Precio) = 0 Then
                'Aplicar descomptes
            Else
                'Aplicar preu net ->dPrecioNeto
                oLin.dto1 = 0
                oLin.dto2 = 0
                oLin.dto3 = 0
                oLin.dto4 = 0
                oLin.dto5 = 0
            End If
            '
        End If
        '
        oLin.dtoTotalCalculado = oDescuento.Calculo_Descuento_Total(oLin.dto1, _
                                                              oLin.dto2, _
                                                              oLin.dto3, _
                                                              oLin.dto4, _
                                                              oLin.dto5)
        '
        If oLin.dtoTotalCalculado = 100 Then
            oLin.Precio = 0
        End If
        ' 
    End Sub
    '
    Private Function ObtenerLinea_SEIDESC(ByVal sSQL As String, _
                                          ByRef oLin As EntDocumentoLin, _
                                          ByRef oCompany As SAPbobsCOM.Company) As Boolean
        '
        Dim oRcs As SAPbobsCOM.Recordset
        '
        ObtenerLinea_SEIDESC = False
        '
        oRcs = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        oRcs.DoQuery(sSQL)
        '
        If Not oRcs.EoF Then
            '
            ObtenerLinea_SEIDESC = True
            '
            oLin.CodigoDescuento = oRcs.Fields.Item("Code").Value
            oLin.dto1 = oRcs.Fields.Item(lDescuento1).Value
            oLin.dto2 = oRcs.Fields.Item(lDescuento2).Value
            oLin.dto3 = oRcs.Fields.Item(lDescuento3).Value
            oLin.dto4 = oRcs.Fields.Item(lDescuento4).Value
            oLin.dto5 = oRcs.Fields.Item(lDescuento5).Value
            oLin.Precio = oRcs.Fields.Item(lPrecioNeto).Value
            '
        End If
        '
        LiberarObjCOM(oRcs)
        '
    End Function


#Region "Funciones Tipos de Datos"

    Private Function Formato_Decimales_IG(ByVal Valor As Object) As String

        Valor = Valor.ToString.Replace(".", "")
        Valor = Valor.ToString.Replace(",", ".")
        Return Valor.ToString

    End Function

    Private Function NullToText(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.ToString.Trim = "" Then
            Return " "
        Else
            Return Valor.ToString
        End If

    End Function

    Private Function NullToInt(ByVal Valor As Object) As Integer

        If IsDBNull(Valor) Or Valor.ToString.Trim = "" Then
            Return 0
        Else
            Return Convert.ToInt32(Valor.ToString)   ' Pasar a integer
        End If

    End Function

    Private Function NullToDoble(ByRef Valor As Object) As Double

        If IsDBNull(Valor) Or Trim(Valor.ToString) = "" Then
            Return 0
        Else
            Return Convert.ToDouble(Valor)  ' Pasar a double
        End If

    End Function

    Private Function NullToData(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.GetType.ToString = "" Then
            Return "NULL"
        Else
            Return String.Format("{0:d}", Valor)
        End If

    End Function

    Private Function NullToHora(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.GetType.ToString = "" Then
            Return "NULL"
        Else
            Return String.Format("{0:t}", Valor)
        End If

    End Function

    Private Function NullToLong(ByVal Valor As Object) As Long

        If IsDBNull(Valor) Or Trim(Valor.ToString) = "" Then
            Return 0
        Else
            Return CType(Valor, Long)   ' Pasar a Long
        End If

    End Function

    Private Function NullToSiNo(ByVal Valor As Object) As String

        If IsDBNull(Valor) Or Valor.GetType.ToString = "" Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function

    Private Function IntToBooleanS_N(ByVal Valor As Object) As String

        If IsDBNull(Valor) Then
            Return "N"
        ElseIf Valor = 0 Then
            Return "N"
        Else
            Return "S"
        End If

    End Function

    Private Function IntToBoolean(ByVal Valor As Object) As String

        If IsDBNull(Valor) Then
            Return "N"
        ElseIf Valor = 0 Then
            Return "N"
        Else
            Return "Y"
        End If

    End Function

    Private Function BooleanToInt(ByVal Valor As Object) As Integer

        If Valor = "True" Then
            Return "1"
        Else
            Return "0"
        End If

    End Function
    '
    Private Function NowDateToString() As String
        NowDateToString = Now.Date.ToString("yyyyMMdd")
    End Function
    '
    ' Poner un valor entre comillas
    Private Function sC(ByVal sValor As String) As String
        sC = "'" & sValor.Replace("'", "''") & "'"
    End Function

#End Region

    '
    Private Sub LiberarObjCOM(ByRef oObjCOM As Object, Optional ByVal bCollect As Boolean = False)
        '
        'Liberar y destruir Objecto com 
        ' En los UDO'S es necesario utilizar GC.Collect  para eliminarlos de la memoria
        If Not IsNothing(oObjCOM) Then
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oObjCOM)
            oObjCOM = Nothing
            If bCollect Then
                GC.Collect()
            End If
        End If

    End Sub


End Class
