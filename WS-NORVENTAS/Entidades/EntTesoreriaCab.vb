﻿Public Class EntTesoreriaCab

    Public Property EmpID As Integer
    Public Property EmpName As String

    Public Property IDIngresoCab As Long
    Public Property FechaIngreso As Integer
    Public Property MedioIngreso As String

    Public Property IDBanco As String
    Public Property NombreBanco As String
    Public Property NumCuenta As String

    Public Property ImporteTotal As Double

    Public Property Sincro As String
    Public Property IncidenciaSincro As String
    Public Property FechaSincro As Integer

End Class
