﻿Public Class EntUsuario

    Public Property EmpID As Integer
    Public Property Name As String
    Public Property Pass As String
    Public Property SuperUser As String
    Public Property SAPUser As Integer
    Public Property SlpCode As Integer
    Public Property ObjetivoMensual As Double
    Public Property ObjetivoAcumulado As Double

    Public Property NumeroPedidosSAPMes As Integer
    Public Property NumeroPedidosSAPDia As Integer
    Public Property NumeroCobrosSAPDia As Integer
    Public Property NumeroCobrosSAPMes As Integer

    Public Property ImporteCobrosSAPDia As Double
    Public Property ImporteCobrosSAPMes As Double
    Public Property ImportePedidosSAPDia As Double
    Public Property ImportePedidosSAPMes As Double

    Public Property AlmacenCentral As String
    Public Property AlmacenUsuario As String

    Public Property ListaPrecioCompra As String
    Public Property ListaPrecioVenta As String

    Public Property Localizable As String
    Public Property CambioRuta As String

    Public Property EmpIDExtra1 As Integer
    Public Property EmpIDExtra2 As Integer

    Public Property NumeracionActualCobro As String
    Public Property NumeracionActualFactura As String
    Public Property NumeracionActualNotaCredito As String

    Public Property Prefijo As String

End Class
