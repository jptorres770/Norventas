﻿Public Class EntTesoreriaLin

    Public Property TipoDoc As String

    Public Property IDIngresoCab As Long
    Public Property IDIngresoLin As Long
    Public Property IDVto As Long
    Public Property DocEntryFra As Integer
    Public Property DocNumFra As String
    Public Property ClienteCode As String
    Public Property ClienteName As String
    Public Property ImporteCobrado As Double
    Public Property ImporteRestante As Double

    Public Property NumTalon As String
    Public Property FechaVtoTalon As Integer

End Class
